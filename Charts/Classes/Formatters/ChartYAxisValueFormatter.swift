//
//  ChartYAxisValueFormatter.swift
//  Charts
//
//  Created by Mike Robinson on 02/08/2016.
//
//  A port of MPAndroidChart for iOS
//  Licensed under Apache License 2.0
//
//  https://github.com/danielgindi/Charts
//

/// An interface for providing custom y-axis Strings.
@objc
public protocol ChartYAxisValueFormatter
{
    
    /// For performance reasons, configure your formatter if any before calling the method
    ///
    /// - returns: the customized String to be drawn.
    /// - parameter value:           the value to be formatted
    ///
    func stringForYValue(value: Double) -> String
    
}